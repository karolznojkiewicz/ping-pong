let player,
    dots = [],
    dotSize = 10,
    scorePlayer = 0,
    scoreAi = 0,
    textS = 30,
    button;

function setup() {
    canvas = createCanvas(windowWidth, windowHeight);
    player = new Player();
    ball = new Ball();
    ai = new Ai();

    for (let i = dotSize / 2; i < height; i += dotSize * 2) {
        dots.push(createVector(width / 2 - dotSize / 2, i));
    }
}

function drawScores() {
    let y = textS * 1.5;

    noStroke();
    fill(255);
    textAlign(CENTER);
    textSize(textS);
    text(scorePlayer, width / 2 - 50, y);
    text(':', width / 2, y);
    text(scoreAi, width / 2 + 50, y);

    gameOver();
}

function draw() {
    if (button) {
        button.remove();
    }
    background(0);
    noStroke();
    fill(255, 255, 255, 40);

    drawSquares();

    player.show();
    player.move();
    ai.show();
    ai.move();
    ball.show();
    ball.move();
    ball.edges();
    ball.scores();
    drawScores();
}

function drawSquares() {
    for (let i = 0; i < dots.length; i++) {
        let x = dots[i].x;
        let y = dots[i].y;

        rect(x, y, dotSize, dotSize + 2);
    }
}

function keyPressed() {
    if (key == 'w' || keyCode === UP_ARROW) {
        player.up();
    } else if (key == 's' || keyCode === DOWN_ARROW) {
        player.down();
    }
}
function keyReleased() {
    if (key != ' ') {
        player.stop();
    }
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function newGame() {
    scorePlayer = 0;
    scoreAi = 0;
    draw();
}

function gameOver() {
    if (scoreAi >= 1 || scorePlayer >= 1) {
        background(0);
        textAlign(CENTER);
        textSize(textS * 5);
        if (scoreAi >= 5) {
            text('YoU LoSt', width / 2, height / 2 - 100);
        } else {
            text('YoU WiN', width / 2, height / 2 - 100);
        }
        button = createButton('New game');
        button.center();
        button.mousePressed(newGame);
    }
}
