function Ball() {
    this.pos = createVector(width / 2, height / 2);
    this.r = 20;
    this.maxSpd = createVector(20, 15);

    do {
        this.acc = p5.Vector.random2D();
        this.acc.setMag(random(4, 6));
    } while (abs(this.acc.x) < 3 || abs(this.acc.y) < 3);

    this.show = function () {
        noStroke();
        fill(255);
        ellipse(this.pos.x, this.pos.y, this.r * 2);
    };

    this.move = function () {
        this.pos.add(this.acc);

        if (this.pos.y < this.r || this.pos.y > height - this.r) {
            this.acc.y *= -1;
        }
    };

    this.edges = function () {
        let collided = false;
        let obj;
        let d1, d2;

        for (let i = 0; i < player.h; i++) {
            d1 = dist(this.pos.x, this.pos.y, ai.pos.x, ai.pos.y + i);
            d2 = dist(
                this.pos.x,
                this.pos.y,
                player.pos.x + player.w,
                player.pos.y + i,
            );

            if (d1 <= this.r) {
                collided = true;
                obj = ai;
                break;
            } else if (d2 <= this.r) {
                collided = true;
                obj = player;
                break;
            }
        }

        if (collided) {
            this.acc.add(createVector(0.5, obj.acc.y));
            this.acc.x *= -1;
            this.acc.x = constrain(this.acc.x, -this.maxSpd.x, this.maxSpd.x);
            this.acc.y = constrain(this.acc.y, -this.maxSpd.y, this.maxSpd.y);
            console.log(this.maxSpd.x, this.maxSpd.y);
        } else {
            collided = false;
        }

        this.scores = function () {
            if (this.pos.x <= this.r) {
                scoreAi++;
                this.reset();
            } else if (this.pos.x >= width - this.r) {
                scorePlayer++;
                this.reset();
            }
        };
        this.reset = function () {
            this.pos = createVector(width / 2, height / 2);
        };
    };
}
